import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import SendbirdBeautymates from 'sendbird-beautymates';
import * as serviceWorker from './serviceWorker';
import './styles.css';

function App() {
  return (
    <div className="container flex items-center justify-center">
      <SendbirdBeautymates
        appId="E22F2016-E8F3-472F-B211-87928AB07E15"
        userId="ihafi83wqufv732gf82"
      />
    </div>
  )
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

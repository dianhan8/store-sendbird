import React from 'react';
import './App.css';
import { useForm } from 'react-hook-form';
import SendBird from 'sendbird';
import lodash from 'lodash';
import moment from 'moment';
export const ID_SENDBIRD = '0F954DE3-6DAE-4B47-91A0-987D27649F1A';

function App() {
  const { handleSubmit, register } = useForm();
  const [message, setMessage] = React.useState([]);
  const refMessage = React.useRef();

  React.useEffect(() => {
    new SendBird({ appId: ID_SENDBIRD });
    listMessage();
    refMessage.current.scrollIntoView({ behavior: "smooth" })
  }, [])


  function pushMessage(message) {
    setMessage(prevState => [...prevState, message]);
  }

  React.useEffect(() => {
    const sb = SendBird.getInstance();
    const CnH = new sb.ChannelHandler();
    CnH.onMessageReceived = function (channel, message) {
      pushMessage(message);
    }
    sb.addChannelHandler('sendbird_open_channel_64795_998343f0519ac3c91aae366a812ec08a6507e0b2', CnH);
  })

  function loadMessage(channel) {
    let messageListQuery = channel.createPreviousMessageListQuery();
    messageListQuery.limit = 50;
    messageListQuery.reverse = false;

    messageListQuery.load(function (messageList, error) {
      if (error) {
        console.log(error)
      } else {
        setMessage(messageList)
      }
    })
  }

  function listMessage() {
    const sb = SendBird.getInstance();
    sb.connect('f4be967eee31ab12ed9bf9f0b4cabbcb8f6c6325', (users, error) => {
      if (error) {
        console.log(error)
      } else {
        sb.OpenChannel.getChannel('sendbird_open_channel_64795_998343f0519ac3c91aae366a812ec08a6507e0b2', (openChannel, error) => {
          openChannel.enter((openChannels, error) => {
            if (error) {
              console.log(error)
            } else {
              loadMessage(openChannel);
            }
          })
        })
      }
    })
  }

  const onSubmit = (e) => {
    const sb = SendBird.getInstance();
    sb.connect('f4be967eee31ab12ed9bf9f0b4cabbcb8f6c6325', (users, error) => {
      if (error) {
        console.log(error)
      } else {
        sb.OpenChannel.getChannel('sendbird_open_channel_64795_998343f0519ac3c91aae366a812ec08a6507e0b2', (openChannel, error) => {
          if (error) {
            console.log(error)
          } else {
            openChannel.enter((openChannels, error) => {
              const text = e.message;
              if (error) {
                console.log(error)
              } else {
                const params = new sb.UserMessageParams;
                params.customType = "store";
                params.message = text;
                openChannel.sendUserMessage(params, (message, error) => {
                  if (error) {
                    console.log(error)
                  } else {
                    pushMessage(message);
                  }
                })
              }
            })
          }
        })
      }
    })
  }

  return (
    <div className="flex items-center justify-center flex-col max-h-screen">

      <div className="w-full">
        <div style={{ width: 400, height: 600 }} className="overflow-y-scroll border border-gray-300">
          <div className="w-full flex flex-col my-2">
            {message.map((item, index) => {
              if (item.customType === 'store') {
                return (
                  <div className="flex items-end justify-end customer my-1" key={item.messageId}>
                    <div className="flex flex-col mr-2 items-end">
                      <span className="font-hind text-xs font-semibold text-gray-500">{lodash.get(item, '_sender.nickname')}</span>
                      <div className="rounded-md p-2 bg-gray-300" style={{ maxWidth: 250 }}>
                        <p className="text-black font-hind text-xs">{item.message}</p>
                      </div>
                      <span className="text-xs text-gray-400 font-medium my-1">
                        {moment(item.createdAt).format('h:mm A')}
                      </span>
                    </div>
                    <img className="lazyload w-8 h-8 rounded-full" src={lodash.get(item, '_sender.profile_url')} />
                  </div>
                )
              } else if (item.customType !== 'store') {
                return (
                  <div className={`flex items-end`} key={item.messageId}>
                    <img className="lazyload w-8 h-8 rounded-full" src={lodash.get(item, '_sender.profile_url')} />
                    <div className="flex flex-col ml-2">
                      <span className="font-hind text-xs font-semibold text-gray-500">{lodash.get(item, '_sender.nickname')}</span>
                      <div className="rounded-md p-2 bg-gray-300" style={{ maxWidth: 250 }}>
                        <p className="text-black font-hind text-xs">{item.message}</p>
                      </div>
                      <span className="text-xs text-gray-400 font-medium">
                        {moment(item.created_at).format('h:mm A')}
                      </span>
                    </div>
                  </div>
                )
              }
            })}
          </div>
          <div ref={refMessage} />
        </div>
        <form onSubmit={handleSubmit(onSubmit)} className="px-2 py-2 flex items-center">
          <input name="message" ref={register} type="text" className="border border-gray-300 rounded focus:outline-none h-10 mr-2" />
          <input type="submit" className="font-hind text-sm p-2 bg-w3 rounded-md rounded text-white font-semibold" />
        </form>
      </div >
    </div>
  );
}

export default App;
